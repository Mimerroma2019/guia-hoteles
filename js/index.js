
  $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
        $('.carousel').carousel({
        interval: 3000
          });

        $("#myModal2").on('show.bs.modal', function (e) {
          console.log('El modal de contacto se abre antes.');
          $('#contactoBtn').removeClass('btn-outline-success');
          $('#contactoBtn').addClass('btn-primary');
          $('#contactoBtn').prop('disabled',true);
        
        });
        $("#myModal2").on('shown.bs.modal', function (e) {
          console.log('El modal de contacto se abre después.');
        });
        $("#myModal2").on('hide.bs.modal', function (e) {
          console.log('El modal de contacto se oculta.');
        });
        $("#myModal2").on('hidden.bs.modal', function (e) {
          console.log('El modal de contacto se ocultó.');
          $('#contactoBtn').prop('disabled',false);
        });
  });
